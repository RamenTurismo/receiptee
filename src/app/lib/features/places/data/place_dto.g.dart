// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'place_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PlaceDto _$PlaceDtoFromJson(Map<String, dynamic> json) => PlaceDto(
      id: json['id'] as String,
    );

PlaceListDto _$PlaceListDtoFromJson(Map<String, dynamic> json) => PlaceListDto(
      total: json['total'] as int,
      items: (json['items'] as List<dynamic>)
          .map((e) => PlaceDto.fromJson(e as Map<String, dynamic>))
          .toList(),
    );
