import 'place_dto.dart';

final class PlacesApi {
  final List<PlaceDto> _somedata =
      List.generate(10000, (index) => PlaceDto(id: 'id #$index'));

  Future<PlaceListDto> search(
      {String? query, required int offset, required int limit}) {
    Iterable<PlaceDto> iterable = _somedata;

    if (query != null) {
      iterable = iterable.where((element) => element.id.contains(query));
    }

    return Future.value(PlaceListDto(
      total: iterable.length,
      items: iterable.skip(offset).take(offset + limit).toList(),
    ));
  }
}
