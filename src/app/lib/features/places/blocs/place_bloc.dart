import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:logger/logger.dart';

import '../models/place_address.dart';
import '../data/place_dto.dart';
import '../data/places_api.dart';
import '../models/place.dart';
import 'place_event.dart';
import 'place_state.dart';

class PlaceBloc extends Bloc<PlacesEvent, PlaceState> {
  final Map<String, PlaceSearchResult> searches = {};
  final PlacesApi _placesApi;
  final Logger _logger;

  PlaceBloc(this._placesApi, this._logger) : super(const PlaceState()) {
    on<PlacesFetched>(_placesFetched);
    on<PlacesFiltered>(_placesFiltered);
  }

  Future<void> _placesFetched(PlacesFetched event, Emitter<PlaceState> emit) {
    return _fetch('', emit);
  }

  Future<void> _placesFiltered(
      PlacesFiltered event, Emitter<PlaceState> emit) async {
    return _fetch(event.value, emit);
  }

  Future<void> _fetch(String query, Emitter<PlaceState> emit) async {
    int offset =
        searches.containsKey(query) ? searches[query]!.places.length : 0;

    _logger.i(
        "Fetching places with query '$query'. Offset=$offset, Searches=${searches.length}");

    // No more things to search, we can emit the state with the cache.
    if (searches.containsKey(query) &&
        offset >= (searches[query]?.total ?? 0)) {
      emit(state.copyWith(
        places: searches[query]!.places,
        currentSearch: query,
      ));
      return;
    }

    PlaceListDto apiResult = await _placesApi.search(
      query: query,
      offset: offset,
      limit: PlaceState.limit,
    );

    List<Place> places = searches
        .putIfAbsent(
          query,
          () => PlaceSearchResult(total: apiResult.total, places: []),
        )
        .places;

    places.addAll(apiResult.items.map(_mapPlaceDto));

    emit(state.copyWith(
      places: places,
      currentSearch: query,
    ));
  }

  static Place _mapPlaceDto(PlaceDto dto) => Place(
        id: dto.id,
        name: 'Burger King',
        address: const PlaceAddress(country: 'France', city: 'Nantes'),
      );
}

class PlaceSearchResult {
  final List<Place> places;
  final int total;

  PlaceSearchResult({
    required this.places,
    required this.total,
  });
}
