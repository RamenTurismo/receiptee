import '../models/place.dart';

class PlaceState {
  static const int limit = 30;

  final String? currentSearch;
  final List<Place> places;

  const PlaceState({
    this.places = const [],
    this.currentSearch,
  });

  PlaceState copyWith({
    String? currentSearch,
    List<Place>? places,
  }) {
    return PlaceState(
      currentSearch: currentSearch ?? this.currentSearch,
      places: places ?? this.places,
    );
  }
}
