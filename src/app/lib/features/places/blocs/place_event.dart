class PlacesEvent {}

final class PlacesFetched extends PlacesEvent {}

final class PlacesFiltered extends PlacesEvent {
  final String value;

  PlacesFiltered(this.value);
}
