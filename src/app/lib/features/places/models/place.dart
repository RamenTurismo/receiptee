import 'package:equatable/equatable.dart';

import 'place_address.dart';

final class Place extends Equatable {
  final String id;
  final String name;
  final PlaceAddress address;

  const Place({required this.id, required this.name, required this.address});

  @override
  List<Object?> get props => [id];
}
