import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../app_localizations.dart';
import '../../../components/bottom_navigation_bar.dart';
import '../../../main.dart';
import '../blocs/place_bloc.dart';
import '../blocs/place_event.dart';
import '../blocs/place_state.dart';
import '../models/place.dart';
import '../widgets/place_card.dart';

class PlaceSearchPage extends StatelessWidget {
  const PlaceSearchPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: const AppBottomNavigationBar(),
      body: BlocProvider(
        create: (_) => PlaceBloc(locator(), locator())..add(PlacesFetched()),
        child: _PlaceSearchView(),
      ),
    );
  }
}

class _PlaceSearchView extends StatefulWidget {
  @override
  State<_PlaceSearchView> createState() => _PlaceSearchViewState();
}

class _PlaceSearchViewState extends State<_PlaceSearchView> {
  final ScrollController _scrollController = ScrollController();
  final TextEditingController _textEditingController = TextEditingController();

  bool get isAtBottom =>
      _scrollController.position.atEdge &&
      _scrollController.position.pixels != 0;

  void _onScroll() {
    if (isAtBottom) {
      context
          .read<PlaceBloc>()
          .add(PlacesFiltered(_textEditingController.text));
    }
  }

  @override
  void initState() {
    super.initState();
    _scrollController.addListener(_onScroll);
  }

  @override
  void dispose() {
    _scrollController
      ..removeListener(_onScroll)
      ..dispose();

    _textEditingController.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<PlaceBloc, PlaceState>(
      builder: (context, state) => Column(
        mainAxisSize: MainAxisSize.max,
        children: [
          searchField(
            AppLocalizations.of(context)!,
            _textEditingController,
            (value) => context.read<PlaceBloc>().add(PlacesFiltered(value)),
          ),
          Expanded(
            child: cardListView(state.places, _scrollController),
          ),
        ],
      ),
    );
  }
}

Widget cardListView(
  List<Place> items,
  ScrollController controller,
) {
  return ListView.builder(
    controller: controller,
    shrinkWrap: true,
    itemCount: items.length,
    itemBuilder: (context, index) {
      Place item = items[index];

      return PlaceCard(place: item);
    },
  );
}

Widget searchField(
  AppLocalizations locale,
  TextEditingController textEditingController,
  void Function(String value) onChanged,
) {
  return TextField(
    decoration: InputDecoration(
      labelText: locale.fieldLabelSearchPlace,
      prefixIcon: const Icon(Icons.search),
    ),
    controller: textEditingController,
    onChanged: onChanged,
  );
}
