class ReceiptModel {
  final List<ReceiptLineModel> lines;

  ReceiptModel({
    required this.lines,
  });
}

class ReceiptLineModel {
  final String productName;
  final double price;

  ReceiptLineModel({
    required this.productName,
    required this.price,
  });
}
