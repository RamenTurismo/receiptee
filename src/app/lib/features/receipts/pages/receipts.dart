import 'package:flutter/material.dart';

import '../../../app_localizations.dart';
import '../../../components/bottom_navigation_bar.dart';
import '../../../main.dart';
import '../../../routes.dart';
import '../data/receipt_api.dart';
import '../data/receipt_dto.dart';

class ReceiptsPage extends StatefulWidget {
  const ReceiptsPage({super.key});

  @override
  State<ReceiptsPage> createState() => _ReceiptsState();
}

class _ReceiptsState extends State<ReceiptsPage> {
  late Future<List<ReceiptTileDto>> futureReceiptTile;
  final ReceiptApi _receiptApi = locator();

  @override
  void initState() {
    super.initState();
    futureReceiptTile = _receiptApi.getAll(page: 0, limit: 0);
  }

  @override
  Widget build(BuildContext context) {
    // Receipt list.
    var futureBuilder = FutureBuilder<List<ReceiptTileDto>>(
      future: futureReceiptTile,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          return _buildListView(snapshot.data!);
        }

        if (snapshot.hasError) {
          return Text('${snapshot.error}');
        }

        if (snapshot.connectionState == ConnectionState.done &&
            !snapshot.hasData) {
          return const SizedBox.shrink();
        }

        return const CircularProgressIndicator();
      },
    );

    // AppBar title, receipt count.
    var appBarBuilder = FutureBuilder<List<ReceiptTileDto>>(
      future: futureReceiptTile,
      builder: (context, snapshot) {
        var locale = AppLocalizations.of(context)!;

        if (snapshot.hasData) {
          return Text(locale.receiptsPageTitle(snapshot.requireData.length));
        }

        if (snapshot.hasError) {
          return Text('${snapshot.error}');
        }

        if (snapshot.connectionState == ConnectionState.done &&
            !snapshot.hasData) {
          return Text(locale.receiptsPageTitle(0));
        }

        return const CircularProgressIndicator();
      },
    );

    return Scaffold(
      bottomNavigationBar: const AppBottomNavigationBar(),
      appBar: AppBar(
        title: appBarBuilder,
      ),
      body: Center(
        child: Column(
          children: <Widget>[
            Expanded(
              child: futureBuilder,
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.pushNamed(context, Routes.receiptsNew);
        },
        tooltip: 'Add a receipt',
        child: const Icon(Icons.add),
      ),
    );
  }

  ListView _buildListView(List<ReceiptTileDto> tiles) {
    return ListView.builder(
      padding: const EdgeInsets.all(8),
      itemCount: tiles.length,
      itemBuilder: (context, index) {
        ReceiptTileDto item = tiles[index];

        return _tile(item, index);
      },
    );
  }

  ListTile _tile(ReceiptTileDto data, int index) {
    return ListTile(
      title: Text(
        "#${data.id.toString()} - ${data.storeName} - Created at ${data.createdAt}",
        style: const TextStyle(
          fontWeight: FontWeight.w500,
          fontSize: 20,
        ),
      ),
      trailing: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          IconButton(
            onPressed: () {},
            icon: const Icon(Icons.edit),
            color: Colors.blue,
          ),
          IconButton(
            onPressed: () {},
            icon: const Icon(Icons.delete),
            color: Colors.red,
          )
        ],
      ),
      tileColor: index % 2 == 0 ? null : const Color(0x22D6D6D6),
      leading: Icon(
        Icons.receipt,
        color: Colors.green[500],
      ),
      onTap: () {
        setState(() {
          // TODO Navigate
        });
      },
    );
  }
}
