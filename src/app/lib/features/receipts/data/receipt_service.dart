import 'receipt_api.dart';
import 'receipt_dto.dart';

class ReceiptService {
  final ReceiptApi _receiptApi;

  const ReceiptService(this._receiptApi);

  Future<List<ReceiptTileDto>> getAll({required int page}) async {
    return await _receiptApi.getAll(page: page, limit: 200);
  }
}
