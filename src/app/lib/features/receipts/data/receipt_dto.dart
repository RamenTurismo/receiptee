import 'package:json_annotation/json_annotation.dart';

part 'receipt_dto.g.dart';

@JsonSerializable(createToJson: false)
class ReceiptTileDto {
  final int id;
  final double totalNet;
  final double totalGross;
  final double totalTaxes;
  final int lineCount;
  final DateTime createdAt;
  final DateTime modifiedAt;
  final String storeLocation;
  final String storeName;

  const ReceiptTileDto({
    required this.id,
    required this.totalNet,
    required this.totalGross,
    required this.totalTaxes,
    required this.lineCount,
    required this.createdAt,
    required this.modifiedAt,
    required this.storeLocation,
    required this.storeName,
  });

  factory ReceiptTileDto.fromJson(Map<String, dynamic> json) =>
      _$ReceiptTileDtoFromJson(json);
}
