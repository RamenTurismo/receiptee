import 'package:flutter/material.dart';
import 'package:receiptee/components/bottom_navigation_bar.dart';
import 'package:receiptee/routes.dart';

class HomePage extends StatelessWidget {
  const HomePage({super.key});

  Widget _buildButton({
    required BuildContext context,
    required Widget icon,
    required Widget text,
    required void Function() onPressed,
  }) {
    return ElevatedButton(
      onPressed: onPressed,
      style: Theme.of(context).elevatedButtonTheme.style,
      child: Container(
        margin: const EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: [
            Flexible(
              child: icon,
            ),
            Flexible(
              child: Center(child: text),
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    List<
        ({
          Widget icon,
          Widget text,
          void Function(BuildContext context) onPressed
        })> items = [
      (
        icon: const Icon(Icons.receipt),
        text: const Text('Receipts'),
        onPressed: (context) {
          Navigator.pushNamed(context, Routes.receipts);
        },
      ),
      (
        icon: const Icon(Icons.add),
        text: const Text('Add a receipt'),
        onPressed: (context) {
          Navigator.pushNamed(context, Routes.receiptsNew);
        },
      ),
    ];

    return Scaffold(
      bottomNavigationBar: const AppBottomNavigationBar(),
      body: Container(
        margin: const EdgeInsets.all(16.0),
        child: ListView.separated(
          itemCount: items.length,
          separatorBuilder: (context, index) => const SizedBox(
            height: 16.0,
          ),
          itemBuilder: (context, index) {
            ({
              Widget icon,
              void Function(BuildContext context) onPressed,
              Widget text,
            }) item = items[index];

            return _buildButton(
              context: context,
              icon: item.icon,
              text: item.text,
              onPressed: () => item.onPressed(context),
            );
          },
        ),
      ),
    );
  }
}
