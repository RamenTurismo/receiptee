class Routes {
  static const receiptsNew = '/receipts/new';
  static const receipts = '/receipts';
  static const places = '/places';
  static const home = '/';
}
