import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:get_it/get_it.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:logger/logger.dart';
import 'package:flutter_web_plugins/url_strategy.dart';

import 'features/places/data/places_api.dart';
import 'features/receipts/pages/receipts.dart';
import 'features/receipts/pages/receipts_new.dart';
import 'features/places/views/place_search_page.dart';
import 'features/receipts/data/receipt_api.dart';
import 'pages/home.dart';
import 'routes.dart';
import 'simple_bloc_observer.dart';

final locator = GetIt.instance;

void main() {
  Logger logger = Logger(
    printer: PrettyPrinter(
      excludeBox: {
        Level.info: true,
        Level.debug: true,
        Level.trace: true,
      },
      methodCount: 0,
      printEmojis: true,
    ),
  );

  GoogleFonts.config.allowRuntimeFetching = false;
  Bloc.observer = SimpleBlocObserver(logger);

  locator.registerSingleton(logger);
  locator.registerLazySingleton(() => ReceiptApi());
  locator.registerLazySingleton(() => PlacesApi());

  logger.i('🎗 Starting receiptee');

  usePathUrlStrategy();

  runApp(const ReceipteeApp());
}

class ReceipteeApp extends StatelessWidget {
  const ReceipteeApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      onGenerateTitle: (context) => AppLocalizations.of(context)!.title,
      theme: ThemeData(
        brightness: Brightness.dark,
        primarySwatch: Colors.blue,
      ),
      localizationsDelegates: AppLocalizations.localizationsDelegates,
      supportedLocales: AppLocalizations.supportedLocales,
      initialRoute: '/',
      routes: {
        '/': (context) => const HomePage(),
        Routes.receipts: (context) => const ReceiptsPage(),
        Routes.receiptsNew: (context) => const ReceiptsNewPage(),
        Routes.places: (context) => const PlaceSearchPage(),
      },
    );
  }
}
