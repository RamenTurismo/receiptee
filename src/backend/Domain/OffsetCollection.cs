﻿namespace Receiptee.Domain;

public record class OffsetCollection<T>(ICollection<T> Items, long Offset, long Total);
