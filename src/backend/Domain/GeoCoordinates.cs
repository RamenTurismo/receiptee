﻿namespace Receiptee.Domain;

public readonly record struct GeoCoordinates(double Latitude, double Longitude);
