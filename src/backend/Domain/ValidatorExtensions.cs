﻿using FluentValidation.Validators;
using FluentValidation;
using System.Runtime.CompilerServices;
using System.Globalization;
using System.Text;
using System;

namespace Receiptee.Domain;

public static class ValidatorExtensions
{
    public static IRuleBuilderOptions<T, string> Alphanumerical<T>(this IRuleBuilder<T, string> ruleBuilder)
        => ruleBuilder.SetValidator(new AlphanumericalValidator<T>());

    public static IRuleBuilderOptions<T, string> ISOCountry<T>(this IRuleBuilder<T, string> ruleBuilder, CultureTypes cultureTypes = CultureTypes.AllCultures)
        => ruleBuilder.Length(3).SetValidator(new CountryValidator<T>(cultureTypes));
}

public sealed class AlphanumericalValidator<T> : PropertyValidator<T, string>
{
    public override string Name => "AlphanumericalValidator";

    public override bool IsValid(FluentValidation.ValidationContext<T> context, string value)
    {
        return value.All(c => char.IsNumber(c) || char.IsLetter(c));
    }
}

public sealed class CountryValidator<T> : PropertyValidator<T, string>
{
    private readonly CultureTypes _cultureTypes;

    public CountryValidator(CultureTypes cultureTypes)
    {
        _cultureTypes = cultureTypes;
    }

    private IEnumerable<string> GetCultures() => CultureInfo.GetCultures(_cultureTypes)
        .Select(c => c.ThreeLetterISOLanguageName)
        .Distinct();

    public override string Name => "CountryValidator";

    public override bool IsValid(FluentValidation.ValidationContext<T> context, string value)
    {
        return GetCultures()
            .Any(e => e.Equals(value, StringComparison.Ordinal));
    }

    protected override string GetDefaultMessageTemplate(string errorCode)
    {
        return new StringBuilder("The country should have a three letter ISO value: ")
            .AppendJoin(", ", GetCultures())
            .ToString();
    }
}