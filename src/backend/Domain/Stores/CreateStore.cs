﻿using Receiptee.Features.Places.Models;

namespace Receiptee.Domain.Stores;

public record CreateStore
{
    public required string Name { get; init; }
    public Address? Address { get; init; }
}
