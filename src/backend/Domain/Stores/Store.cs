﻿namespace Receiptee.Domain.Stores;

public sealed record Store : CreateStore
{
    public required StoreId Id { get; init; }
}
