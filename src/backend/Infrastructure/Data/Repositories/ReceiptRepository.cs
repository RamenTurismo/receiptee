﻿namespace Receiptee.Infrastructure.Data.Repositories;

internal sealed class ReceiptRepository
{
    private readonly IMongoCollection<ReceiptDb> _receipts;

    public ReceiptRepository(IMongoDatabase db)
    {
        _receipts = db.GetCollection<ReceiptDb>("receipts");
    }

    public Task InsertAsync(ReceiptDb[] receipts, CancellationToken cancellationToken = default)
    {
        if (receipts.Length == 0) return Task.CompletedTask;

        if (receipts.Length == 1)
        {
            return _receipts.InsertOneAsync(receipts[0], cancellationToken: cancellationToken);
        }

        return _receipts.InsertManyAsync(receipts, cancellationToken: cancellationToken);
    }
}
