﻿namespace Receiptee.Infrastructure.Data.Repositories;

internal sealed class PlaceRepository : IPlaceRepository
{
    private readonly IMongoCollection<PlaceEntity> _places;

    public PlaceRepository(IMongoDatabase db)
    {
        _places = db.GetCollection<PlaceEntity>("place");
    }

    public async ValueTask<OffsetCollection<PlaceEntity>> GetAllAsync(int offset, int limit, CancellationToken cancellationToken = default)
    {
        IFindFluent<PlaceEntity, PlaceEntity> query = _places.Find(FilterDefinition<PlaceEntity>.Empty)
            .Skip(offset)
            .Limit(limit);

        long total = await query.CountDocumentsAsync(cancellationToken);
        List<PlaceEntity> items = await query.ToListAsync(cancellationToken);

        return new OffsetCollection<PlaceEntity>(items, offset, total);
    }
}
