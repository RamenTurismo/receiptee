﻿namespace Receiptee.Infrastructure.Data;

public static class ServiceCollectionExtensions
{
    public static IServiceCollection AddMongoDb(this IServiceCollection services, IConfiguration configuration)
    {
        BsonSerializer.RegisterSerializationProvider(new SerializerProvider());

        services.AddSingleton<IMongoDatabase>(provider =>
        {
            MongoClient client = new(MongoClientSettings.FromConnectionString(configuration.GetConnectionString("MongoDb")));

            return client.GetDatabase("receiptee");
        });

        services.AddScoped<IPlaceRepository, PlaceRepository>();

        return services;
    }
}
