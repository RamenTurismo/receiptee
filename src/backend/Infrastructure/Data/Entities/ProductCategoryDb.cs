﻿namespace Receiptee.Infrastructure.Data.Entities;

public sealed record ProductCategoryDb
{
    public ICollection<ProductDb> Products { get; set; }

    public int Id { get; set; }

    public string Label { get; set; } = null!;

    public ProductCategoryDb()
    {
        Products = new HashSet<ProductDb>();
    }
}
