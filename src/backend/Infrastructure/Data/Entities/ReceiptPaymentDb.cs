﻿using Receiptee.Features.Payments;

namespace Receiptee.Infrastructure.Data.Entities;

public sealed record ReceiptPaymentDb
{
    public PaymentKind PaymentKind { get; set; }

    public PaymentDb? Payment { get; set; }

    public int ReceiptId { get; set; }

    public ReceiptDb? Receipt { get; set; }
}
