﻿namespace Receiptee.Infrastructure.Data.Entities;

public sealed class AddressEntity
{
    [BsonRequired]
    public required string Country { get; set; }
    [BsonRequired]
    public required string City { get; set; }
    [BsonIgnoreIfNull]
    public string? ZipCode { get; set; }
    [BsonIgnoreIfNull]
    public string? AddressLineOne { get; set; }
    [BsonIgnoreIfNull]
    public string? AddressLineTwo { get; set; }
}
