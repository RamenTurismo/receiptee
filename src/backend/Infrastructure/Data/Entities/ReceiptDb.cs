﻿using Receiptee.Api.Receipts;

namespace Receiptee.Infrastructure.Data.Entities;

public class ReceiptDb
{
    public ICollection<ReceiptLineDb> Lines { get; set; }

    public ICollection<ReceiptPaymentDb> Payments { get; set; }

    public ReceiptId Id { get; set; }

    public DateTime CreatedAt { get; set; }

    public DateTime PaidAt { get; set; }

    public StoreDb Store { get; set; } = null!;

    public Guid StoreId { get; set; }

    public ReceiptDb()
    {
        Lines = new HashSet<ReceiptLineDb>();
        Payments = new HashSet<ReceiptPaymentDb>();
    }
}
