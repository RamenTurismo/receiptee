﻿namespace Receiptee.Infrastructure.Data.Entities;

public sealed class PlaceEntity
{
    [BsonId]
    public required PlaceId Id { get; set; }
    [BsonRepresentation(BsonType.DateTime)]
    public required DateTimeOffset CreatedAt { get; set; }
    [BsonRepresentation(BsonType.DateTime)]
    public required DateTimeOffset UpdatedAt { get; set; }
    [BsonRequired]
    public required string Name { get; set; }
    [BsonIgnoreIfNull]
    public required AddressEntity Address { get; set; }
}

[StronglyTypedId(jsonConverter: StronglyTypedIdJsonConverter.SystemTextJson, backingType: StronglyTypedIdBackingType.Guid)]
public partial struct PlaceId
{
}