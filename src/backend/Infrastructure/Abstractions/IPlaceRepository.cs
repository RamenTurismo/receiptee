﻿namespace Receiptee.Infrastructure.Abstractions;

public interface IPlaceRepository
{
    ValueTask<OffsetCollection<PlaceEntity>> GetAllAsync(int offset, int limit, CancellationToken cancellationToken = default);
}
