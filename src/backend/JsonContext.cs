﻿using Receiptee.Features.Receipts;

namespace Receiptee;

[JsonSerializable(typeof(ReceiptType))]
public partial class JsonContext : JsonSerializerContext
{
}
