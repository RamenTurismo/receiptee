﻿using Receiptee.Features.Places.Models;

namespace Receiptee.Features.Places;

public sealed class GetAllPlacesRequest
{
    [FromQuery]
    public required int Offset { get; set; }
    [FromQuery]
    public required int Limit { get; set; }
}

public sealed record GetAllPlacesResponse(
    long Total,
    IEnumerable<Place> Places);

[JsonSerializable(typeof(GetAllPlacesRequest))]
[JsonSerializable(typeof(GetAllPlacesResponse))]
internal partial class GetAllPlacesSerializerContext : JsonSerializerContext;

public sealed class GetAllPlacesMapper : ResponseMapper<GetAllPlacesResponse, OffsetCollection<PlaceEntity>>
{
    private readonly PlaceEntityMapper _placeEntityMapper = new();

    public override GetAllPlacesResponse FromEntity(OffsetCollection<PlaceEntity> e)
    {
        return new GetAllPlacesResponse(e.Total, e.Items.Select(_placeEntityMapper.FromEntity));
    }
}

internal sealed class PlaceEntityMapper : ResponseMapper<Place, PlaceEntity>
{
    private readonly AddressEntityMapper _addressEntityMapper = new();

    public override Place FromEntity(PlaceEntity e)
    {
        return new Place
        {
            Id = Guid.Empty,
            Name = e.Name,
            Address = _addressEntityMapper.FromEntity(e.Address)
        };
    }
}

internal sealed class AddressEntityMapper : ResponseMapper<Address, AddressEntity>
{
    public override Address FromEntity(AddressEntity e)
    {
        return new Address
        {
            Country = e.Country,
            AddressLineOne = e.AddressLineOne,
            AddressLineTwo = e.AddressLineTwo,
            City = e.City,
            ZipCode = e.ZipCode
        };
    }
}

public sealed class GetAllPlacesEndPoint(IPlaceRepository placeRepository) : Endpoint<GetAllPlacesRequest, GetAllPlacesResponse, GetAllPlacesMapper>
{
    public override void Configure()
    {
        Get("/places");

        SerializerContext(GetAllPlacesSerializerContext.Default);
    }

    public override async Task HandleAsync(GetAllPlacesRequest req, CancellationToken ct)
    {
        OffsetCollection<PlaceEntity> places = await placeRepository.GetAllAsync(req.Offset, req.Limit, ct);

        GetAllPlacesResponse response = Map.FromEntity(places);

        await SendOkAsync(response, ct);
    }
}
