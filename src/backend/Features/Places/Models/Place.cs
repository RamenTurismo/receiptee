﻿namespace Receiptee.Features.Places.Models;

public sealed record Place
{
    public required Guid Id { get; init; }
    public required string Name { get; init; }
    public required Address Address { get; init; }
    public float? KilometersFromUser { get; set; }
}
