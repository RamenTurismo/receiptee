﻿namespace Receiptee.Features.Places.Models;

public sealed record Address
{
    public required string Country { get; set; }
    public required string City { get; set; }
    public string? ZipCode { get; set; }
    public string? AddressLineOne { get; set; }
    public string? AddressLineTwo { get; init; }
}
