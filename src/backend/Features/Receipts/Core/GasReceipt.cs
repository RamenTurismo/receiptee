﻿using Receiptee.Features.Payments;
using Receiptee.Features.Products;

namespace Receiptee.Features.Receipts.Core;

public class GasReceipt
{
    public required StoreId StoreId { get; set; }

    public required PaymentKind Payment { get; set; }

    public required DateTime PaidAt { get; set; }

    public Product? Fuel { get; set; }

    public required int Quantity { get; set; }
}
