﻿using Receiptee.Features.Payments;
using Receiptee.Products;

namespace Receiptee.Receipts;

public record CreateReceiptRequest
{
    public required DateTime PaidAt { get; init; }

    public required StoreId StoreId { get; init; }

    public required ICollection<PaymentKind> Payments { get; init; }

    public required ICollection<CreateReceiptLineRequest> Lines { get; init; }
}

public record CreateReceiptLineRequest
{
    /// <summary>
    /// PrimaryKey part.
    /// </summary>
    public required ushort RowNumber { get; init; }

    public required string Label { get; init; }

    public required int Quantity { get; init; }

    public required decimal PriceNet { get; init; }

    public required string VatCode { get; set; }

    public required ProductId ProductId { get; init; }
}