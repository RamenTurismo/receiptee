﻿using Receiptee.Features.Products;

namespace Receiptee.Features.Receipts;

public sealed record ReceiptLine
{
    /// <summary>
    /// PrimaryKey part.
    /// </summary>
    public ushort RowNumber { get; set; }

    public required string Label { get; set; }

    public int Quantity { get; set; }

    /// <summary>
    /// Without taxes.
    /// </summary>
    public decimal PriceNet { get; set; }

    /// <summary>
    /// With taxes.
    /// </summary>
    public decimal PriceGross { get; set; }

    public required string VatCode { get; set; }

    public required Product Product { get; set; }
}
