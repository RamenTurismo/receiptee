﻿using FluentValidation;
using Receiptee.Features.Places.Models;

namespace Receiptee.Features.Stores;

public sealed class PostStoreValidator : Validator<CreateStore>
{
    public PostStoreValidator()
    {
        RuleFor(e => e.Name)
            .NotEmpty()
            .MinimumLength(3);

        When(e => e.Address is not null, () =>
        {
            RuleFor(e => e.Address!).SetValidator(new AddressValidator());
        });
    }
}

public sealed class AddressValidator : AbstractValidator<Address>
{
    public AddressValidator()
    {
        When(e => e.StreetNumber is not null, () =>
        {
            RuleFor(e => e.StreetNumber)
                .NotEmpty()
                .MinimumLength(1)
                .Alphanumerical();
        });

        RuleFor(e => e.Country)
            .ISOCountry();
    }
}