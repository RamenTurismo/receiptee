﻿using Receiptee.Features.Places.Models;

namespace Receiptee.Features.Stores.Mappers;

public sealed class AddressMapper : Mapper<Address, Address, AddressDb>
{
    public override Address FromEntity(AddressDb e)
    {
        return new Address
        {
            City = e.City,
            Country = e.Country,
            PostalCode = e.PostalCode,
            Region = e.Region,
            Street = e.Street,
            StreetNumber = e.StreetNumber
        };
    }

    public override AddressDb ToEntity(Address r)
    {
        return new AddressDb
        {
            City = r.City,
            Country = r.Country,
            PostalCode = r.PostalCode,
            Region = r.Region,
            Street = r.Street,
            StreetNumber = r.StreetNumber
        };
    }
}