﻿namespace Receiptee.Features.Stores.Mappers;

public sealed class CreateStoreMapper : Mapper<CreateStore, Store, StoreDb>
{
    private readonly AddressMapper _addressMapper;
    private readonly TimeProvider _timeProvider;
    private readonly StoreMapper _storeMapper;

    public CreateStoreMapper(AddressMapper addressMapper, TimeProvider timeProvider, StoreMapper storeMapper)
    {
        _addressMapper = addressMapper;
        _timeProvider = timeProvider;
        _storeMapper = storeMapper;
    }

    public override Store FromEntity(StoreDb e) => _storeMapper.FromEntity(e);

    public override StoreDb ToEntity(CreateStore r)
    {
        return new StoreDb
        {
            Id = StoreId.New(),
            CreatedAt = _timeProvider.GetUtcNow(),
            UpdatedAt = _timeProvider.GetUtcNow(),
            Name = r.Name,
            Address = r.Address == null ? null : _addressMapper.ToEntity(r.Address),
        };
    }
}

