﻿namespace Receiptee.Features.Stores.Mappers;

public sealed class StoreMapper : ResponseMapper<Store, StoreDb>
{
    private readonly AddressMapper _addressMapper;

    public StoreMapper(AddressMapper addressMapper)
    {
        _addressMapper = addressMapper;
    }

    public override Store FromEntity(StoreDb e)
    {
        return new Store
        {
            Id = e.Id,
            Name = e.Name,
            Address = e.Address == null ? null : _addressMapper.FromEntity(e.Address),
        };
    }
}
