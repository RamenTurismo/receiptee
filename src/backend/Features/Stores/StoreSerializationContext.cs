﻿namespace Receiptee.Features.Stores;

[JsonSourceGenerationOptions(PropertyNamingPolicy = JsonKnownNamingPolicy.CamelCase)]
[JsonSerializable(typeof(OffsetCollection<Store>))]
[JsonSerializable(typeof(CreateStore))]
[JsonSerializable(typeof(Store))]
[JsonSerializable(typeof(ErrorResponse))]
public partial class StoreSerializationContext : JsonContext
{
}