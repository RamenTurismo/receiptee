﻿namespace Receiptee.Features.Stores;

public sealed class PostStore : Endpoint<CreateStore, Store, CreateStoreMapper>
{
    private readonly IStoreRepository _storeRepository;

    public PostStore(IStoreRepository storeRepository)
    {
        _storeRepository = storeRepository;
    }

    public override void Configure()
    {
        Post("stores");

        SerializerContext(StoreSerializationContext.Default);
    }

    public override async Task HandleAsync(CreateStore req, CancellationToken ct)
    {
        StoreDb entity = Map.ToEntity(req);

        await _storeRepository.InsertAsync(entity, ct);

        Store model = Map.FromEntity(entity);

        await SendOkAsync(model, ct);
    }
}