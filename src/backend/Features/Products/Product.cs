﻿namespace Receiptee.Features.Products;

public class Product
{
    /// <summary>
    /// Can be <c>default</c> to indicate a new product.
    /// </summary>
    public int Id { get; set; }

    public required string Name { get; set; }

    public required string CategoryCode { get; set; }
}
