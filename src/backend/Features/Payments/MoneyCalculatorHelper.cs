﻿namespace Receiptee.Features.Payments;

public static class MoneyCalculatorHelper
{
    public static decimal GetTotalGross(in decimal priceNoTaxes, in int quantity, in decimal taxes)
    {
        return priceNoTaxes * quantity * (taxes / 100);
    }

    public static decimal GetTotalNet(in decimal priceWithTaxes, in int quantity, in decimal taxes)
    {
        return priceWithTaxes * quantity * (taxes / 100);
    }
}
