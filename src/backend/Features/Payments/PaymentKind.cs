﻿namespace Receiptee.Features.Payments;

public enum PaymentKind
{
    Unknown = 0,
    CreditCard = 1,
    RestaurantCreditCard = 2,
    Cash = 3
}
