using Receiptee.Features.Stores.Mappers;

WebApplicationBuilder builder = WebApplication.CreateBuilder(args);

builder.Services.AddFastEndpoints()
    .SwaggerDocument();

// Database

builder.Services.AddMongoDb(builder.Configuration);

builder.Services.AddAuthorization();
builder.Services.AddAuthentication();

builder.Services.TryAddSingleton<AddressMapper>();
builder.Services.TryAddSingleton<StoreMapper>();

WebApplication app = builder.Build();

app.UseHttpsRedirection();

app.UseAuthorization();

app.UseFastEndpoints(c =>
    {
        c.Versioning.Prefix = "v";
    })
   .UseSwaggerGen();

app.Run();